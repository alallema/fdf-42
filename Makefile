
NAME = fdf

IDIR = ./incs/
INCS = libft.h	\
	   fdf.h

SDIR = ./srcs/
SRCS = fdf.c			\
	   print_map.c		\
	   new_image.c		\
	   draw_line.c		\
	   ft_lstcreate.c	\
	   ft_lstpushback.c	\

SRCC = $(addprefix $(SDIR),$(SRCS))

OBJS = $(SRCS:.c=.o)

FLAG = -Wall -Werror -Wextra

$(NAME):
	@make -C ./libft/
	gcc $(FLAG) -c $(SRCC) -I$(IDIR)
	gcc $(FLAG) $(OBJS) ./libft/libft.a -L/usr/local/lib/ -I/usr/local/include -lmlx -framework OpenGL -framework AppKit -o $(NAME)

all: $(NAME)

clean:
	/bin/rm -f $(OBJS)

fclean: clean
	@make -C ./libft/ fclean
	/bin/rm -f $(NAME)

re: fclean all
