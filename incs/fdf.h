/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 17:34:47 by alallema          #+#    #+#             */
/*   Updated: 2016/04/30 17:01:57 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <mlx.h>
# include "libft.h"
# define X (elem->tab[i]->x)
# define X1 (elem->tab[i + 1]->x)
# define X2 (elem->next->tab[i]->x)
# define Y (elem->tab[i]->y)
# define Y1 (elem->tab[i + 1]->y)
# define Y2 (elem->next->tab[i]->y)
# define Z (elem->tab[i]->z)
# define Z1 (elem->tab[i + 1]->z)
# define Z2 (elem->next->tab[i]->z)

typedef struct		s_point
{
	int				x;
	int				y;
	int				z;
	int				i;
	char			*color;
}					t_point;

typedef struct		s_lst
{
	int				size;
	t_point			**tab;
	struct s_lst	*next;
}					t_lst;

typedef struct		s_img
{
	char			*adr;
	int				bpp;
	int				size;
	int				end;
	void			*img;
}					t_img;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	int				zoom;
	float			rot;
	float			rotx;
	float			roty;
	int				movx;
	int				movy;
	t_img			*img;
	t_lst			lst;
}					t_env;

t_point				*ft_create_sp(t_lst *elem, int i, int j, t_env *e);
t_point				*ft_create_sp2(t_lst *elem, int i, int j, t_env *e);
char				*create_color(int z);
void				clear_img(t_img *img);
void				key_funct(int keycode, t_env *e);
void				draw_line(t_point *t, t_env *e);
t_img				*create_img(t_env *e);
void				pixel_put_to_image(t_env *e, int x, int y, int color);
t_lst				*ft_lstcreate(t_point **tab, int i);
void				ft_lstpushback(t_lst **begin, t_point **tab, int i);
t_lst				*ft_read_map(t_lst *lst, char *line, int fd);

#endif
