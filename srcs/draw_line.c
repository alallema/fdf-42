/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 19:41:15 by alallema          #+#    #+#             */
/*   Updated: 2016/04/30 19:00:13 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_draw_line1(t_point *d, t_point *inc, t_env *e, t_point *p)
{
	inc->i = 2 * d->y - d->x;
	d->z = 2 * (d->y - d->x);
	inc->z = 2 * d->y;
	while (d->i < d->x)
	{
		if (inc->i >= 0)
		{
			p->y += inc->y;
			inc->i += d->z;
		}
		else
			inc->i += inc->z;
		p->x += inc->x;
		if (p->color)
			pixel_put_to_image(e, p->x, p->y, ft_atoi_base16(p->color));
		else
			pixel_put_to_image(e, p->x, p->y, ft_atoi_base16(p->color));
		d->i++;
	}
}

void		ft_draw_line2(t_point *d, t_point *inc, t_env *e, t_point *p)
{
	inc->i = 2 * d->x - d->y;
	d->z = 2 * (d->x - d->y);
	inc->z = 2 * d->x;
	while (d->i < d->y)
	{
		if (inc->i >= 0)
		{
			p->x += inc->x;
			inc->i += d->z;
		}
		else
			inc->i += inc->z;
		p->y += inc->y;
		if (p->color)
			pixel_put_to_image(e, p->x, p->y, ft_atoi_base16(p->color));
		else
			pixel_put_to_image(e, p->x, p->y, ft_atoi_base16(p->color));
		d->i++;
	}
}

void		draw_line(t_point *t, t_env *e)
{
	t_point	d;
	t_point	inc;
	t_point	p;

	if (t->color)
		p.color = ft_strdup(t->color);
	else
		p.color = NULL;
	p.x = t->x;
	p.y = t->y;
	d.x = t->z - p.x;
	d.y = t->i - p.y;
	d.i = 0;
	d.x < 0 ? d.x = -d.x : d.x;
	d.y < 0 ? d.y = -d.y : d.y;
	inc.x = 1;
	if (t->z < p.x)
		inc.x = -1;
	inc.y = 1;
	if (t->i < p.y)
		inc.y = -1;
	if (d.x > d.y)
		ft_draw_line1(&d, &inc, e, &p);
	else
		ft_draw_line2(&d, &inc, e, &p);
}

t_point		*ft_create_sp(t_lst *elem, int i, int j, t_env *e)
{
	t_point	*p;

	p = (t_point *)malloc(sizeof(t_point));
	p->x = (X - j * e->rot * e->rotx) * e->zoom + e->movx - Z * 2;
	p->y = (Y + i * e->rot * e->roty) * e->zoom + e->movy - Z * 2;
	p->z = (X1 - j * e->rot * e->rotx) * e->zoom + e->movx - Z1 * 2;
	p->i = (Y1 + (i + 1) * e->rot * e->roty) * e->zoom + e->movy - Z1 * 2;
	if (elem->tab[i]->color)
		p->color = elem->tab[i]->color;
	else
		p->color = create_color(Z);
	return (p);
}

t_point		*ft_create_sp2(t_lst *elem, int i, int j, t_env *e)
{
	t_point	*p;

	p = (t_point *)malloc(sizeof(t_point));
	p->x = (X - j * e->rot * e->rotx) * e->zoom + e->movx - Z * 2;
	p->y = (Y + i * e->rot * e->roty) * e->zoom + e->movy - Z * 2;
	p->z = (X2 - (j + 1) * e->rot * e->rotx) * e->zoom + e->movx - Z2 * 2;
	p->i = (Y2 + i * e->rot * e->roty) * e->zoom + e->movy - Z2 * 2;
	if (elem->tab[i]->color)
		p->color = elem->tab[i]->color;
	else
		p->color = create_color(Z);
	return (p);
}
