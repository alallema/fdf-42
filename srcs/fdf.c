/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 19:39:53 by alallema          #+#    #+#             */
/*   Updated: 2016/04/30 19:06:01 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

char			*ft_check_co(char *s, char *dest)
{
	int			i;

	i = 0;
	while (s[i] && s[i] != ',')
		i++;
	if (s[i])
	{
		dest = (char *)malloc(sizeof(char) * 7);
		dest = ft_strncpy(dest, &s[i + 3], 7);
		return (dest);
	}
	else
		return (NULL);
}

int				ft_check_tmp(char *s)
{
	int			i;
	int			j;

	i = 0;
	j = 0;
	while (s[i] && s[i] != ',')
	{
		if (s[i] == '-' || s[i] == '+')
			i++;
		if (s[i] < 48 || s[i] > 57)
			return (0);
		i++;
	}
	if (s[i] == ',')
	{
		if ((s[i] > 102 && s[i] != 'x') || (s[i] < 48 && s[i] != 44))
			return (0);
		if ((s[i] < 65 && s[i] > 57) || (s[i] > 70 && s[i] < 97))
			return (0);
		i++;
		j++;
	}
	if (j > 9)
		return (0);
	return (1);
}

int				create_tab(t_point **tab, char **tmp, int i, int j)
{
	while (tmp[i])
	{
		if (ft_check_tmp(tmp[i]) == 0)
			return (-1);
		tab[i] = (t_point *)malloc(sizeof(t_point));
		tab[i]->x = i;
		tab[i]->y = j;
		tab[i]->z = ft_atoi(tmp[i]);
		tab[i]->color = ft_check_co(tmp[i], tab[i]->color);
		i++;
	}
	return (i);
}

t_lst			*ft_read_map(t_lst *lst, char *line, int fd)
{
	t_point		**tab;
	char		**tmp;
	int			i;
	int			j;

	j = 0;
	while (get_next_line(fd, &line) > 0)
	{
		i = 0;
		if (!(tmp = ft_strsplit(line, ' ')))
			return (NULL);
		while (tmp[i])
			i++;
		tab = (t_point **)malloc(sizeof(t_point *) * (i + 1));
		i = 0;
		if ((i = create_tab(tab, tmp, i, j)) == -1)
			return (NULL);
		tab[i] = NULL;
		if (!lst)
			lst = ft_lstcreate(tab, i);
		else
			ft_lstpushback(&lst, tab, i);
		j++;
	}
	return (lst);
}
