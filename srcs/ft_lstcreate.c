/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcreate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/23 12:47:57 by alallema          #+#    #+#             */
/*   Updated: 2016/04/29 11:05:46 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_lst		*ft_lstcreate(t_point **tab, int i)
{
	t_lst	*elem;

	elem = (t_lst *)malloc(sizeof(t_lst));
	if (elem)
	{
		elem->tab = tab;
		elem->size = i;
		elem->next = NULL;
	}
	return (elem);
}
