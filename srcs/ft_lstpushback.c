/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushback.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/21 18:11:44 by alallema          #+#    #+#             */
/*   Updated: 2016/04/29 11:05:15 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_lstpushback(t_lst **begin, t_point **tab, int i)
{
	t_lst	*elem;

	elem = *begin;
	if (elem != NULL)
	{
		while (elem->next)
			elem = elem->next;
	}
	elem->next = ft_lstcreate(tab, i);
}
