/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_image.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/30 12:17:28 by alallema          #+#    #+#             */
/*   Updated: 2016/04/30 16:57:49 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

char	*create_color(int z)
{
	if (z <= -10)
		return ("000099");
	if (z < 0 && z > -10)
		return ("0033CC");
	if (z == 0)
		return ("FFFFFF");
	if (z > 0 && z <= 10)
		return ("33FF99");
	if (z >= 10 && z < 20)
		return ("FFFF99");
	if (z >= 20 && z < 30)
		return ("FF9966");
	if (z >= 30 && z < 40)
		return ("FF3333");
	if (z >= 40 && z < 50)
		return ("FF3399");
	if (z > 50 && z < 100)
		return ("CC00CC");
	if (z > 100)
		return ("660066");
	return ("FFFFFF");
}

void	key_funct(int keycode, t_env *e)
{
	if (keycode == 13)
		e->roty -= 0.2;
	if (keycode == 1)
		e->roty += 0.2;
	if (keycode == 2)
		e->rotx -= 0.2;
	if (keycode == 0)
		e->rotx += 0.2;
	if (keycode == 24)
		e->zoom += 1;
	if (keycode == 27)
		e->zoom -= 1;
	if (keycode == 126)
		e->movy -= 5;
	if (keycode == 125)
		e->movy += 5;
	if (keycode == 123)
		e->movx -= 5;
	if (keycode == 124)
		e->movx += 5;
	if (keycode == 7)
		e->rot -= 0.2;
	if (keycode == 6)
		e->rot += 0.2;
}

void	clear_img(t_img *img)
{
	int	i;

	i = 1920 * 1080 * 4 - 1;
	ft_bzero(img->adr, i);
}

void	pixel_put_to_image(t_env *e, int x, int y, int color)
{
	int		i;
	int		bpp;
	int		sizeline;
	char	*data;

	bpp = e->img->bpp;
	sizeline = e->img->size;
	data = e->img->adr;
	if (x < 1920 && y < 1080 && x > 0 && y > 0)
	{
		i = x * (bpp / 8) + y * sizeline;
		data[i] = color % 256;
		color /= 256;
		data[i + 1] = color % 256;
		color /= 256;
		data[i + 2] = color % 256;
	}
}

t_img	*create_img(t_env *e)
{
	t_img	*img;

	img = (t_img *)malloc(sizeof(t_img));
	img->img = mlx_new_image(e->mlx, 1920, 1080);
	img->bpp = 0;
	img->size = 0;
	img->end = 0;
	img->adr = mlx_get_data_addr(img->img, &img->bpp, &img->size, &img->end);
	return (img);
}
