/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alallema <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/23 16:04:09 by alallema          #+#    #+#             */
/*   Updated: 2016/04/30 19:37:50 by alallema         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			draw_funct(t_env *e)
{
	t_lst	*elem;
	int		i;
	int		j;

	i = 0;
	j = 0;
	elem = &e->lst;
	while (elem)
	{
		i = 0;
		while (elem->tab[i + 1])
		{
			draw_line(ft_create_sp(elem, i, j, e), e);
			if (elem->next)
				draw_line(ft_create_sp2(elem, i, j, e), e);
			i++;
		}
		if (elem->next && i < elem->next->size)
			draw_line(ft_create_sp2(elem, i, j, e), e);
		elem = elem->next;
		j++;
	}
	return (0);
}

int			menu(t_env *e)
{
	mlx_string_put(e->mlx, e->win, 5, 20, 0xFFFFFF, "zoom + : +");
	mlx_string_put(e->mlx, e->win, 5, 40, 0xFFFFFF, "zoom - : -");
	mlx_string_put(e->mlx, e->win, 5, 60, 0xFFFFFF,
	"deplacement : fleche haut");
	mlx_string_put(e->mlx, e->win, 5, 80, 0xFFFFFF, "deplacement : fleche bas");
	mlx_string_put(e->mlx, e->win, 5, 100, 0xFFFFFF,
	"deplacement : fleche gauche");
	mlx_string_put(e->mlx, e->win, 5, 120, 0xFFFFFF,
	"deplacement : fleche droite");
	mlx_string_put(e->mlx, e->win, 5, 140, 0xFFFFFF,
	"rotation sens horaire : X");
	mlx_string_put(e->mlx, e->win, 5, 160, 0xFFFFFF,
	"rotation sens anti-horaire: Z");
	mlx_string_put(e->mlx, e->win, 5, 180, 0xFFFFFF, "perspective : A - D");
	mlx_string_put(e->mlx, e->win, 5, 200, 0xFFFFFF, "perspective : W - S");
	return (0);
}

int			close_funct(int keycode, t_env *e)
{
	if (keycode == 53)
	{
		mlx_destroy_image(e->mlx, e->img->img);
		mlx_destroy_window(e->mlx, e->win);
		exit(0);
	}
	key_funct(keycode, e);
	clear_img(e->img);
	draw_funct(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img->img, 0, 0);
	menu(e);
	return (0);
}

void		print_map(t_lst *lst)
{
	t_env	e;

	e.lst = *lst;
	e.zoom = 10;
	e.rot = 1;
	e.rotx = 0.5;
	e.roty = 0.5;
	e.movy = 100;
	e.movx = 800;
	e.mlx = mlx_init();
	e.win = mlx_new_window(e.mlx, 1920, 1080, "fdf");
	e.img = create_img(&e);
	draw_funct(&e);
	mlx_put_image_to_window(e.mlx, e.win, e.img->img, 0, 0);
	menu(&e);
	mlx_hook(e.win, 2, 0, close_funct, &e);
	mlx_loop(e.mlx);
}

int			main(int ac, char **av)
{
	int		fd;
	t_lst	*lst;
	char	*line;

	lst = NULL;
	line = ft_strnew(1);
	fd = 0;
	if (ac != 2)
		ft_putstr("wrong number file");
	if (ac == 2)
	{
		fd = open(av[1], O_RDONLY);
		if (fd == -1)
			ft_putstr("erreur");
		if (!(lst = ft_read_map(lst, line, fd)))
			ft_putstr("invalid file");
		if (ac == 2 && fd != -1 && lst)
		{
			print_map(lst);
			close(fd);
		}
	}
	return (0);
}
